all: probe find

find:
	@echo /dev/input/$$(basename $$(ls -d $$(find $$(dirname $$(find 2>/dev/null /sys -name get_icons)) -name input?*)/event*))

probe: probe.c
	gcc -I/usr/include/libevdev-1.0 -o $@ $< -levdev
