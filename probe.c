/* Listen for the Yealink USB P1K phone and connect it to BareSIP.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <linux/input.h>

#include <libevdev/libevdev.h>


bool baresip (pid_t *child, int *infd, int *outfd) {
	int pin [2], pout [2];
	if ((pipe (pin) < 0) || (pipe (pout) < 0)) {
		return false;
	}
	pid_t pid = fork ();
	switch (pid) {
	case -1:
		/* Error */
		return false;
	case 0:
		/* Child */
		dup2 (pin  [0], 0);
		dup2 (pout [1], 1);
		close (pin  [1]);
		close (pout [0]);
		execlp ("/usr/local/bin/baresip", "baresip", NULL);
		perror ("Child failed to start BareSIP");
		exit (1);
	default:
		/* Parent */
		*infd  = pin  [1];
		*outfd = pout [0];
		close (pin  [0]);
		close (pout [1]);
		*child = pid;
		return true;
	}
}


int main (int argc, char *argv []) {
	char *program = argv [0];
	//
	// Redirect stdout (as well as stderr)
	int tmpout = open ("/tmp/yealink.log", O_RDWR | O_CREAT | O_APPEND, 0660);
	if (tmpout < 0) {
		perror ("Failed to open output logfile");
		system ("wall Failed to open output logfile");
		exit (1);
	}
	dup2 (tmpout, 1);
	dup2 (tmpout, 2);
	close (tmpout);
	tmpout = -1;
	setlinebuf (stdout);
	setlinebuf (stderr);
	//
	// Fetch the device name
	char *evdev = getenv ("DEVNAME");
	if (evdev == NULL) {
		fprintf (stderr, "Please set $DEVNAME, behaving like udev would\n");
		exit (1);
	}
	printf ("Device name is %s\n", evdev);
#if 0
	//
	// Check arguments
	if (argc != 2) {
		fprintf (stderr, "Usage: %s /dev/input/event123\n", program);
		exit (1);
	}
	char *evdev = argv [1];
#endif
	//
	// Open the input event device
	int fd = open (evdev, O_RDWR);
	if (fd < 0) {
		perror ("Failed to open input event device");
		exit (1);
	}
	//
	// Construct the libevdev structure
	struct libevdev *dev = NULL;
	if (libevdev_new_from_fd (fd, &dev) < 0) {
		fprintf (stderr, "Failed to access input event device with libevdev\n");
		close (fd);
		exit (1);
	}
	//
	// Grab ownership of the input event device
	if (libevdev_grab (dev, LIBEVDEV_GRAB) != 0) {
		fprintf (stderr, "Failed to grab the input event device\n");
		libevdev_free (dev);
		close (fd);
		exit (1);
	}
	//
	// Start the child process running BareSIP
	pid_t child = -1;
	int chin = -1, chout = -1;
	if (!baresip (&child, &chin, &chout)) {
		exit (1);
	}
	//
	// Process events
	do {
		struct input_event iev;
		if (libevdev_next_event (dev, LIBEVDEV_READ_FLAG_NORMAL, &iev) != 0) {
			fprintf (stderr, "Error reading events\n");
			break;
		}
		printf("Event: %s %s %d\n",
				libevdev_event_type_get_name (iev.type),
				libevdev_event_code_get_name (iev.type, iev.code),
				iev.value);
	} while (true);
	//
	// Report success
	printf ("Yay, I owned it!\n");
	//
	// Cleanup and exit
	libevdev_free (dev);
	close (fd);
	return 0;
}

