# USB Phone to BareSIP

> *The program `usb2sip` connects USB phones to a SIP backend for online telephony.*

There are a few forms of USB-pluggable phones that require a computer program
to allow digital telephony.  They might be "specially certified for" some
commercial lock-in network.  That's not how telephony is supposed to be, so
this program unleashes the wealth of SIP telephony.

To do this, it relies on BareSIP as a client, both for configuring accounts
and other desired properties and for subscribing, answering and so on.  The
USB phone is a mere audio device with some buttons and often a display.

The facilitation of these USB phones is quite nice -- you might find one in
a place where you are visiting, and you can plug them into your desktop
(if you trust that it will not take over your computer!) to use a more
direct shape; the shape that you cusomtarily ignore until it calls for
your attention, or until you need it.


## Structure of the Program

You need to setup a script in your `/etc/udev/rules.d` directory, which will
recognise your USB phone when it is added and start this prgram.  Removal of
the USB phone can be detected, and leads to it closing off properly, after
terminating any outstanding calls.

The program runs as a deamon for as long as the USB phone is plugged in.
You should be able to use it almost immediately, and start dialing or
simply start working until it rings.

The program has two kinds of plugins:

  * USB phone plugins with the specific code for some phone models;
    this helps to focus on one model, or just a few models, instead of
    always building a monolith.

  * Platform plugins capture variability between POSIX platforms.
    This will mostly come down to USB handling variations.
    Note that Windows has become increasingly supportive of POSIX, so it
    too might benefit from this program (but we won't take the plunge
    into its jungle of handles).

This program is designed for the Linux USB infrastructure.  If you would
like to make it work on another platform, please send a merge request so
it may be integrated.  Try to avoid most `#if` style variations, and try
to accommodate automatic detection with in the `CMakeLists.txt`.  The
program stays clean if we move the platform code into separate files.
Please discuss if you want parts of the program reorganised.


## Varieties of USB Phones

This program picks up the identifiers of the USB phone, and selects the
corresponding handling procedure.  Usually, this means recognising the keys
being pressed and sending useful information to the display.

USB Phones do need explicit support for this to work.  The models below
are currently supported.

**General Remarks:**
You must always configure BareSIP as instructed in its documentation.
The example configuration files should be helpful.  By default, `usb2sip`
runs as `root`, so setup BareSIP in that account.

BareSIP offers ways for remote connections.  Since it runs as a child process
this is not necessary for `usb2sip`, but it may be helpful during setup
to look around for registration or call status, or error conditions.


### Yealink USB-P1K / Tiptel 115

These look like a simple GSM phone with an attached USB cable.  There is a
simply keybboard and an LCD screen.

Your kernel may have an outdated driver, in which case you can install a
[DKMS module](https://github.com/treitmayr/yealink-module)
to replace the driver module.

Then add a file `/etc/udev/rules.d/99-yealink-usb-p1k.rules` containing

```
SUBSYSTEM=="input", ACTION=="add", ENV{ID_BUS}=="usb", ENV{ID_USB_DRIVER}=="yealink", ENV{ID_VENDOR_ID}=="6993", ENV{ID_MODEL_ID}=="b001", PROGRAM=="/path/to/usb2sip"
```

Now (remove and) plugin the USB phone and enjoy your flight.


### Other models

The general idea of a USB phone appears to be a combination of a sound
device with an input device and often something to drive a display.
Much of this uses general USB handling infrastructure in Linux.

When your device is not supported above, you may be pleased to find that
there is a fallback handler that explicitly logs input events, so you can
figure out what translation would be sensible for your phone.  To add a
new phone, copy a similar model and edit until you have it working.

Once done, please share your findings; they are likely to benefit all.
